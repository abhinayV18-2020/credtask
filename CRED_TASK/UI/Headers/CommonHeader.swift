//
//  CommonHeader.swift
//  CRED_TASK
//
//  Created by abhinay varma on 24/04/21.
//

import UIKit

class CommonHeader: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var arrowButton: UIButton!
    var cardType:String?
    var isExpandedCallBack:(()->Void)?
    var selectedModel:HeaderData?
    
    init() {
        super.init(frame: CGRect.zero)
        self.setContentHuggingPriority(.defaultHigh, for: .vertical)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func updateData(_ model:HeaderData) {
        self.titleLabel.text = model.titleText
        self.descriptionLabel.text = model.descriptionText
        selectedModel = model
    }
    
    func loadView() -> UIView {
        let bundleName = Bundle(for: type(of: self))
        let nibName = String(describing: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundleName)
        let contentView = nib.instantiate(withOwner: nil, options: nil).first as! CommonHeader
        contentView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(contentView)
        return contentView
    }
    
    @IBAction func arrowAction(_ sender: Any) {
        hideUnhideArrowButton()
        self.isExpandedCallBack?()
    }
    
    func hideUnhideArrowButton() {
        let flag = self.arrowButton.isHidden
        self.arrowButton.isHidden = !flag
        if flag {
            switch cardType {
            case "CardType1" : self.titleLabel.text = "credit amount"
                self.descriptionLabel.text = "₹1,50,000"
                break
            case "CardType2" : self.titleLabel.text = "EMI"
                self.descriptionLabel.text = "₹4,247/mo"
                break
            default :
                break
            }
        }else {
            self.titleLabel.text = selectedModel?.titleText ?? ""
            self.descriptionLabel.text = selectedModel?.descriptionText ?? ""
        }
    }
    
    func onExpansion(_ isExpanded:Bool) {
        self.alpha = isExpanded ? 1 : 0.2
    }

}
