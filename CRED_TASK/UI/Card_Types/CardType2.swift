//
//  CardType2.swift
//  CRED_TASK
//
//  Created by abhinay varma on 24/04/21.
//

import UIKit

class CardType2: CustomView {
    @IBOutlet weak var paymentTypesCollectionView: UICollectionView!
    @IBOutlet weak var ctaButton:UIButton!
    @IBOutlet weak var dynamicDistance: NSLayoutConstraint!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.cardType = "CardType2"
    }
    
    override init() {
        super.init()
    }
    
    func loadView() -> UIView {
        let bundleName = Bundle(for: type(of: self))
        let nibName = String(describing: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundleName)
        let contentView = nib.instantiate(withOwner: nil, options: nil).first as! UIView
        contentView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(contentView)
        return contentView
    }
    
    override func updateData(_ forCard:CardContent?) {
        paymentTypesCollectionView.register(UINib(nibName: "CardTypePaymentOptionsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CardTypePaymentOptionsCollectionViewCell")
        paymentTypesCollectionView.dataSource = self
        paymentTypesCollectionView.delegate = self
        self.ctaButton.setTitle(forCard?.ctaTitle, for: .normal)
        
        setupDynamicConstrants()
    }
    
    private func setupDynamicConstrants() {
        let height = UIScreen.main.bounds.height
        
        if height > 667{
            
            dynamicDistance.constant = 150
            
        }else if height == 667{
            
            dynamicDistance.constant = 30
            
        }else{
            
            //iPhone SE UI Settings
            dynamicDistance.constant = 30
        }
    }
    
    @IBAction func onClickCTA(_ sender: Any) {
        self.isExpandedCallBack?()
    }

    
    override func hideViewsOnCollapse() {
        self.alpha = 0.0
        self.ctaButton.isHidden = true
    }
    
    override func showViewOnExpansion() {
        self.alpha = 1.0
        self.ctaButton.isHidden = false
    }
}

extension CardType2:UICollectionViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardTypePaymentOptionsCollectionViewCell", for: indexPath) as? CardTypePaymentOptionsCollectionViewCell
        let color = (indexPath.item == 0 ? UIColor(rgb: 0x4D3942) : (indexPath.item == 1 ? UIColor(rgb: 0x877D98) : UIColor(rgb: 0x607899)))
        cell?.contentView.backgroundColor = color
        cell?.recommendedHeaderView.isHidden = indexPath.item != 1
      
        cell?.selectionButton.backgroundColor = indexPath.item == 0 ? color.mix(with: .black, amount: 0.3) : .clear
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.frame.size.width - 44.0)/2.5
        return CGSize(width: width, height: collectionView.frame.size.height - 40.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}
