//
//  CardType3.swift
//  CRED_TASK
//
//  Created by abhinay varma on 24/04/21.
//

import UIKit

class CardType3: CustomView {
    
    @IBOutlet weak var ctaButton:UIButton!
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.cardType = "CardType3"
    }
    
    override init() {
        super.init()
    }
    
    func loadView() -> UIView {
        let bundleName = Bundle(for: type(of: self))
        let nibName = String(describing: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundleName)
        let contentView = nib.instantiate(withOwner: nil, options: nil).first as! UIView
        contentView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(contentView)
        return contentView
    }
    
    override func updateData(_ forCard:CardContent?) {
        self.ctaButton.setTitle(forCard?.ctaTitle, for: .normal) 
    }
    
    @IBAction func onClickCTA(_ sender: Any) {
        self.isExpandedCallBack?()
    }
    
    override func hideViewsOnCollapse() {
        self.alpha = 0.0
        self.ctaButton.isHidden = true
    }
    
    override func showViewOnExpansion() {
        self.alpha = 1.0
        self.ctaButton.isHidden = false
    }

}
