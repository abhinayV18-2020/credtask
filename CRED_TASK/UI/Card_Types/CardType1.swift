//
//  CardType1.swift
//  CRED_TASK
//
//  Created by abhinay varma on 24/04/21.
//

import UIKit

class CardType1: CustomView {
    @IBOutlet weak var ctaButton:UIButton!
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var dynamicDistance: NSLayoutConstraint!
    
    var constraintsOfMarker:[NSLayoutConstraint]?
    
    var marker:UIView!
    
    override init() {
        super.init()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.cardType = "CardType1"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        mainContentView.layer.cornerRadius = 20.0
        setupMarker()
        setupCircularView()
    }
    
    private func setupCircularView() {
        circularView.layer.cornerRadius = circularView.bounds.size.height / 2
        circularView.layer.masksToBounds = true
        circularView.clipsToBounds = false
    }
    
    private func setupMarker() {
        marker.layer.cornerRadius = 30.0
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panHandler(_:)))
        marker.addGestureRecognizer(panGesture)
        let bgColor = circularView.layer.borderColor
        marker.layer.borderWidth = 8.0
        marker.layer.borderColor = bgColor
        marker.backgroundColor = UIColor.black
    }
    
    func loadView() -> UIView {
        let bundleName = Bundle(for: type(of: self))
        let nibName = String(describing: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundleName)
        let contentView = nib.instantiate(withOwner: nil, options: nil).first as! UIView
        contentView.translatesAutoresizingMaskIntoConstraints = false
        setupMarkerOnCircularView(contentView as! CardType1)
        self.addSubview(contentView)
        return contentView
    }
    
    override func updateData(_ forCard:CardContent?) {
        self.ctaButton.setTitle(forCard?.ctaTitle, for: .normal)
        setupDynamicConstraints()
    }
    
    private func setupDynamicConstraints() {
        let height = UIScreen.main.bounds.height
        
        if height > 667{
            
            dynamicDistance.constant = 90
            
        }else if height == 667{
            
            dynamicDistance.constant = 10
            
        }else{
            
            //iPhone SE UI Settings
            dynamicDistance.constant = 10
        }
    }
    
    func setupMarkerOnCircularView(_ contentView:CardType1) {
        contentView.marker = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 60.0, height: 60.0))
        contentView.marker.translatesAutoresizingMaskIntoConstraints = false
        contentView.marker.backgroundColor = .green
        
        contentView.addSubview(contentView.marker)
        
        contentView.marker.widthAnchor.constraint(equalToConstant: 60).isActive = true
        contentView.marker.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        let (hMult, vMult) = computeMultipliers(angle: 45)
        
        // position the little green circle using a multiplier on the right and bottom
        let constraint1 = NSLayoutConstraint(item: contentView.marker!, attribute: .centerX, relatedBy: .equal, toItem:contentView.circularView!, attribute: .trailing, multiplier: hMult, constant: contentView.circularView.layer.cornerRadius - 10)
        let constraint2 = NSLayoutConstraint(item: contentView.marker!, attribute: .centerY, relatedBy: .equal, toItem: contentView.circularView!, attribute: .bottom, multiplier: vMult, constant: -contentView.circularView.layer.cornerRadius + 60)
      
        constraint1.isActive = true
        constraint2.isActive = true
        contentView.constraintsOfMarker = [constraint1,constraint2]
    }
    
    func computeMultipliers(angle: CGFloat) -> (CGFloat, CGFloat) {
          let radians = angle * .pi / 180
          
          let h = (1.0 + cos(radians)) / 2
          let v = (1.0 - sin(radians)) / 2
          
          return (h, v)
    }
    
    @IBAction func ctaAction(_ sender: Any) {
        self.isExpandedCallBack?()
    }
    
    override func hideViewsOnCollapse() {
        self.alpha = 0.0
        self.ctaButton.isHidden = true
    }
    
    override func showViewOnExpansion() {
        self.alpha = 1.0
        self.ctaButton.isHidden = false
    }
    
    @objc func panHandler(_ gesture:UIPanGestureRecognizer) {
        let point = gesture.location(in: self)
        updateForPoints(point)
    }
    
    func updateForPoints(_ point: CGPoint) {

        /*
         * Parametric equation of circle
         * x = a + r cos t
         * y = b + r sin ⁡t
         * a, b are center of circle
         * t (theta) is angle
         * x and y will be points which are on circumference of circle
         *
                   270
                    |
                _   |   _
                    |
         180 -------o------- 360
                    |
                +   |   +
                    |
                   90
         *
         */

        let centerOffset =  CGPoint(x: point.x - circularView.frame.midX, y: point.y - circularView.frame.midY)

        let a: CGFloat = circularView.center.x
        let b: CGFloat = circularView.center.y
        let r: CGFloat = circularView.layer.cornerRadius
        let theta: CGFloat = atan2(centerOffset.y, centerOffset.x)
        
        let newPoints = CGPoint(x: a + r * cos(theta), y: b + r * sin(theta))

        var rect = marker.frame
        rect.origin = newPoints
        marker.center = newPoints
    }
    
}
