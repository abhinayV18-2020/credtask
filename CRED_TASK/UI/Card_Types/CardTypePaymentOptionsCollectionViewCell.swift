//
//  CardTypePaymentOptionsCollectionViewCell.swift
//  CRED_TASK
//
//  Created by abhinay varma on 26/04/21.
//

import UIKit

class CardTypePaymentOptionsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var recommendedHeaderView: UIView!
    @IBOutlet weak var paymentOptionTitle: UILabel!
    @IBOutlet weak var selectionButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        recommendedHeaderView.layer.masksToBounds = false
        recommendedHeaderView.layer.zPosition = 1
        self.contentView.layer.cornerRadius = 20.0
        self.contentView.layer.masksToBounds = false
        self.layer.masksToBounds = false
    }

}
