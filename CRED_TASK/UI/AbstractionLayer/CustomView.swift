//
//  CustomView.swift
//  CRED_TASK
//
//  Created by abhinay varma on 24/04/21.
//

import UIKit

class CustomView:UIView {
    var cardType:String?
    var isExpandedCallBack:(()->Void)?
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    init() {
        super.init(frame: CGRect.zero)
    }
    
    func hideViewsOnCollapse() {}
    
    func showViewOnExpansion() {}
    
    func updateData(_ forCard:CardContent?){}
}
