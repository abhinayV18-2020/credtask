//
//  CustomOuterStackView.swift
//  CRED_TASK
//
//  Created by abhinay varma on 24/04/21.
//

import UIKit

class CustomOuterStackView: UIStackView {
    
    //MARK: Properties
    private var innerStackViews:[CustomInnerStackView]?
    private var lastSelectedIndex = 0
    
    //MARK: Initializers
    init() {
        super.init(frame: CGRect(x: 0.0, y: Constants.headerHeight, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height - Constants.headerHeight))
        setupStackViews()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        self.backgroundColor = UIColor(red: 17.0/255.0, green: 35.0/255/0, blue: 50.0/255.0, alpha: 1.0)
    }
    
    private func setupStackViews() {
        self.axis = .vertical
        self.distribution = .fill
        self.alignment = .fill
        self.spacing = 0
    }
    
    func addSubViewsToInnerStackViews(subViews:[CustomInnerStackView]) {
        innerStackViews = subViews
        addArrangedSubview(innerStackViews!.first!)
        callbacks()
    }
    
    //MARK: Delegation of data from child views to innerStackView to outerStackView
    private func callbacks() {
        for innerView in innerStackViews ?? [] {
            innerView.delegate = self
        }
    }
}

//MARK Contains delegate method for expanding and all animations related code.
extension CustomOuterStackView:ExpansionDelegate {
    //MARK: Delgation of data from innserstackview on click of CT
    func onExpand(_ cardType: String) {
        var index:Int?
        for counter in 0..<innerStackViews!.count {
            let subview:CustomView? = innerStackViews?[counter].expandableCard
            if(subview?.cardType == cardType) {
                index = counter
                break
            }
        }
        if index != nil && index! < innerStackViews!.count - 1 {
            addRemoveStackViews(index!)
            lastSelectedIndex = lastSelectedIndex > index! ? index! : index! + 1
        }
    }
    
    
    private func addRemoveStackViews(_ forIndex:Int) {
        let selectedInnerStackView:CustomInnerStackView? = self.innerStackViews?[forIndex]
        let previousSelectedInnerStackView:CustomInnerStackView? = self.innerStackViews?[self.lastSelectedIndex]
        let nextSelectedInnerStackViewToExpand:CustomInnerStackView? = self.innerStackViews?[forIndex + 1]
        if(forIndex < lastSelectedIndex) {
            selectedInnerStackView?.expandOnClickFromHigherIndexToLow()
            previousSelectedInnerStackView?.hideAndRemoveAllCardsFromTheBottomOfTheViewClicked(completion: {
                [weak self,forIndex,lastSelectedIndex] in
                self?.removeViewsFromBottomOfTheSelectedIndexes(forIndex+1, lastSelectedIndex)
            })
        }else {
            nextSelectedInnerStackViewToExpand?.setupInitialFrameBeforeExpanding()
            self.addArrangedSubview(nextSelectedInnerStackViewToExpand!)
            
            let cardViewPresent = checkIfCardTypeViewPresentInTheView(nextSelectedInnerStackViewToExpand)
            
            previousSelectedInnerStackView?.contractAndReduce({[weak self,lastSelectedIndex] in
                self?.removeLastExpandedCardViewFromInnerStackViews(lastSelectedIndex)
            })
            
            nextSelectedInnerStackViewToExpand?.expandStackViewOnClickOnCTAS(cardViewPresent, forIndex)
        }
    }
    
    
    private func removeViewsFromBottomOfTheSelectedIndexes(_ startIndex:Int , _ lastSelIndex:Int) {
        for index in startIndex...lastSelIndex {
            let innerView:CustomInnerStackView? = self.innerStackViews?[index]
            innerView?.removeFromSuperview()
            if index != lastSelIndex
            {
            (self.innerStackViews![lastSelIndex]).expandableCard?.isHidden = false
            }
            self.layoutIfNeeded()
        }
    }
    
    
    private func checkIfCardTypeViewPresentInTheView(_ nextSelectedInnerStackViewToExpand:CustomInnerStackView?) -> Bool {
        for tView in nextSelectedInnerStackViewToExpand?.subviews ?? [] {
            if(tView.isKind(of: CustomView.self)) {
                return true
            }
        }
        return false
    }
    
    
    private func removeLastExpandedCardViewFromInnerStackViews(_ index:Int) {
        (innerStackViews?[index])?.expandableCard?.removeFromSuperview()
        (innerStackViews?[index])?.expandableCard?.isHidden = false
        self.layoutIfNeeded()
    }
}
