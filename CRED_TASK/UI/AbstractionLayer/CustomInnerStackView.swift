//
//  CustomStackView.swift
//  CRED_TASK
//
//  Created by abhinay varma on 24/04/21.
//

import UIKit


protocol ExpansionDelegate:class {
    func onExpand(_ cardType:String)
}

class CustomInnerStackView:UIStackView {
    
    //MARK:Properties
    weak var delegate:ExpansionDelegate?
    private var isExpanded:Bool?
    var expandableCard:CustomView?
    var headerView:CommonHeader?
    
    //MARK: initial view setup
    override func layoutSubviews() {
        super.layoutSubviews()
        applyCornerRadius()
    }
    
    private func applyCornerRadius() {
        self.layer.cornerRadius = 20.0
        self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.layer.masksToBounds = true
    }
    
    private func setupStackView() {
        self.axis = .vertical
        self.distribution = .fill
        self.alignment = .fill
        self.spacing = 0
    }
    
    //MARK: Card And Header addition
    func addSubViewsToInnerStackViews(subViews:[UIView]) {
        setupStackView()
        for view in subViews {
            addArrangedSubview(view)
        }
        expandableCard = subViews[1] as? CustomView
        headerView = subViews[0] as? CommonHeader
        headerView?.hideUnhideArrowButton()
        initiateCallbacks()
    }
    
    //MARK: On click of CTA in card and expanding arrow in header
    private func initiateCallbacks() {
        expandableCard?.isExpandedCallBack = {
            self.delegate?.onExpand(self.expandableCard?.cardType ?? "")
        }
        
        headerView?.isExpandedCallBack = {
            self.delegate?.onExpand(self.expandableCard?.cardType ?? "")
        }
    }
}

//MARK: Animation methods triggered from outerstackview
extension CustomInnerStackView {
    //MARK:On Click on arrows lastSelectedIndex > selectedIndex
    func expandOnClickFromHigherIndexToLow() {
        addArrangedSubview(expandableCard!)
        UIView.animate(
            withDuration: 0.4,
            delay: 0.0,
            options: [.curveEaseIn],
            animations: { [weak self]() in
                self?.expandableCard?.isHidden = false
                self?.expandableCard?.showViewOnExpansion()
                self?.headerView?.onExpansion(true)
            },completion: nil
        )
    }
    
    func contractAndReduce(_ completion:(()->())?) {
        UIView.animate(
            withDuration: 0.4,
            delay: 0.0,
            options: [.curveEaseOut],
            animations: {[weak self]() in
                self?.expandableCard?.hideViewsOnCollapse()
                self?.headerView?.onExpansion(false)
                self?.headerView?.hideUnhideArrowButton()
                
                self?.expandableCard?.isHidden = true
                self?.layoutIfNeeded()
            },completion: { [weak self](_) in
                self?.headerView?.onExpansion(true)
                self?.expandableCard?.showViewOnExpansion()
                completion?()
            }
        )
    }
    
    //MARK:On Click on arrows lastSelectedIndex > selectedIndex remo
    func hideAndRemoveAllCardsFromTheBottomOfTheViewClicked(completion:(()->())?) {
        UIView.animate(
            withDuration: 0.4,
            delay: 0.0,
            options: [.curveEaseIn],
            animations: { [weak self]() in
                self?.expandableCard?.hideViewsOnCollapse()
                self?.headerView?.onExpansion(false)
                var prevSelframe = self?.frame
                prevSelframe?.origin.y = UIScreen.main.bounds.size.height
                self?.expandableCard?.isHidden = true
                self?.frame = prevSelframe ?? CGRect.zero
                self?.layoutIfNeeded()
            },completion: { [weak self](_) in
                self?.headerView?.onExpansion(true)
                self?.expandableCard?.showViewOnExpansion()
                completion?()
            }
        )
    }
    
    func setupInitialFrameBeforeExpanding() {
        var newframe = self.frame
        newframe.origin.y = UIScreen.main.bounds.size.height
        newframe.size.width = UIScreen.main.bounds.size.width
        self.frame = newframe
        self.layoutIfNeeded()
    }
    
    func expandStackViewOnClickOnCTAS(_ cardPresent:Bool, _ index:Int) {
        UIView.animate(
            withDuration: 0.4,
            delay: 0.0,
            options: [.curveLinear],
            animations: {[weak self]() in
                if !(cardPresent) {
                    self?.headerView?.hideUnhideArrowButton()
                    self?.addArrangedSubview((self?.expandableCard!)!)
                }
                var newframe = self?.frame ?? CGRect.zero
                newframe.origin.y = Constants.headerHeight * CGFloat(index+1)
                self?.frame = newframe
                self?.expandableCard?.isHidden = false
                
                self?.expandableCard?.showViewOnExpansion()
                self?.headerView?.onExpansion(true)
                self?.layoutIfNeeded()
            },completion: nil
        )
    }
    
}
