//
//  Models.swift
//  CRED_TASK
//
//  Created by abhinay varma on 24/04/21.
//

import Foundation



struct HeaderData:Codable {
    var titleText:String?
    var descriptionText:String?
}

struct CardModel:Codable {
    var headerData:HeaderData?
    var cardData:CardContent?
    var cardType:String?
}

struct CardData:Codable {
    var cards:[CardModel]?
}

struct CardContent:Codable {
    var ctaTitle:String?
    var isExpanded:Bool?
}



