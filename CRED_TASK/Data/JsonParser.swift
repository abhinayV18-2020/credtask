//
//  JsonParser.swift
//  CRED_TASK
//
//  Created by abhinay varma on 25/04/21.
//

import Foundation
import AVFoundation

class JSONParser {
    typealias result<T> = (Result<T, Error>) -> Void

    func downloadList<T: Decodable>(of _: T.Type,
                                    from data: Data,
                                    completion: @escaping result<T>) {
        do {
            let decodedData: T = try JSONDecoder().decode(T.self, from: data)
            completion(.success(decodedData))
        } catch {
            completion(.failure(error))
        }
    }
}
