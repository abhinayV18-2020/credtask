//
//  ViewModel.swift
//  CRED_TASK
//
//  Created by abhinay varma on 25/04/21.
//

import Foundation

class ViewModel {
    
    var headers:[HeaderData]?
    
    var cards:[CardModel]? {
        didSet {
            let newHeaders:[HeaderData]? = cards?.map({ (model) -> HeaderData in
                model.headerData!
            })
            headers = newHeaders ?? []
        }
    }
    
    
    var dataSource:CardData? {
        didSet {
            cards = dataSource?.cards
        }
    }
    
    
    func returnHeadersForTheStaticJson() -> [CommonHeader] {
        var outputHeaders:[CommonHeader] = []
        for header in headers ?? [] {
            let commonHeader = CommonHeader().loadView() as! CommonHeader
            commonHeader.translatesAutoresizingMaskIntoConstraints = false
            let index = headers?.firstIndex(where: { (newHeader) -> Bool in
                return newHeader.titleText == header.titleText
            })
            commonHeader.cardType = cards?[index ?? 0].cardType ?? ""
            commonHeader.updateData(header)
            outputHeaders.append(commonHeader)
        }
        return outputHeaders
    }
    
    
    func returnCardsForTheStaticJson() -> [CustomView] {
        var outputCards:[CustomView] = []
        for card in cards ?? [] {
            let genericCard:CustomView?
            switch card.cardType {
            case "CardType1" :
                genericCard = CardType1().loadView() as! CardType1
                break
            case "CardType2" :
                genericCard = CardType2().loadView() as! CardType2
                break
            case "CardType3":
                    genericCard = CardType3().loadView() as! CardType3
                    break
            default :
                genericCard = CardType3().loadView() as! CardType3
                break
            }
            genericCard?.translatesAutoresizingMaskIntoConstraints = false
            genericCard?.updateData(card.cardData)
            outputCards.append(genericCard!)
        }
        return outputCards
    }
    
    
    
    
}
