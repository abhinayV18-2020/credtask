//
//  ViewController.swift
//  CRED_TASK
//
//  Created by abhinay varma on 24/04/21.
//

import UIKit

class ViewController: UIViewController {
    
    let viewModel = ViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchSampleData()
    }
    
    //MARK: Populating Models based on a sample JSON
    func fetchSampleData() {
        let url = Bundle.main.url(forResource: "CardsData", withExtension: "json")!
        let data = try! Data(contentsOf: url)
        let genericParser = JSONParser()
        genericParser.downloadList(of: CardData.self, from: data) { result in
            switch result {
            case let .failure(error):
                print(error.localizedDescription)
            case let .success(cardData):
                print(cardData)
                self.viewModel.dataSource = cardData
                self.loadViewsOnUI()
            }
        }
    }
    
    func loadViewsOnUI() {
        let outerStackView = CustomOuterStackView()
        outerStackView.translatesAutoresizingMaskIntoConstraints = false

        let headerViews = viewModel.returnHeadersForTheStaticJson()
        let cardViews = viewModel.returnCardsForTheStaticJson()
        var innerStackViews:[CustomInnerStackView] = []
        
        for index in 0..<headerViews.count {
            let innerstackView = CustomInnerStackView()
            innerstackView.translatesAutoresizingMaskIntoConstraints = false
            headerViews[index].heightAnchor.constraint(equalToConstant: Constants.headerHeight).isActive = true
            headerViews[index].backgroundColor = cardViews[index].backgroundColor
            innerstackView.addSubViewsToInnerStackViews(subViews: [headerViews[index],cardViews[index]])
            innerStackViews.append(innerstackView)
            innerstackView.layer.cornerRadius = 30.0
        }
        
        view.addSubview(outerStackView)
        outerStackView.addSubViewsToInnerStackViews(subViews: innerStackViews)
        
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: outerStackView.topAnchor, constant: -UIScreen.main.bounds.size.height*0.16),
            view.bottomAnchor.constraint(equalTo: outerStackView.bottomAnchor, constant: 0),
            view.leftAnchor.constraint(equalTo: outerStackView.leftAnchor, constant: 0),
            view.rightAnchor.constraint(equalTo: outerStackView.rightAnchor, constant: 0)
        ])
    }
}

