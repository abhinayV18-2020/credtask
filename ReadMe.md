//Documentation 

# Project Structure 
Project is divided into following 5 main layers -:

- ### Data
This layer has a json parser and different models consumed in the application.

- ### Utilities
This layer consists of generic extensions and constants user in the project.

- ### UI
this layer has all set of UI designed in the app.
this is further divided into following 3 parts -:
1. Abstraction Layer
This consist of the stack view implementation of the current UI. It has mainly 2 files outercollectionview and innercollectionview.
2. , 3. Custom Views (Cards and Headers)
which is been passed in the abstraction layer to populate the entire UI.

- ### ProjectFiles
This contains resources , plist , storyboard , appdelegate etc.

- ### MainScene
This layer contains the usage of the abstraction layer.
Mainly contains the ViewController and ViewModel in which I'm reading a hardcoded JSON file
to populate my model objects and use them in making the content of the UI dynamic.



## Usage of abstraction layer -:
initialize outer stack view and inner stack views.
add your custom headerview and cellview in inner stack views and then add all of the inner stack views to outer stack view.
```
let outerView = OuterStackView()
let innerView1 = InnerStackView()
let innerView2 = InnerStackView()
let innerView3 = InnerStackView()

innerView1.addSubViews(customHeader1,customCell1)
innerView1.addSubViews(customHeader1,customCell2)
innerView1.addSubViews(customHeader1,customCell3)

outerView.addSubViews(innerView1,innerView2,innerView3)
self.view.addSubview(outerView)
```

